import React, {Component} from 'react';
import ramda from 'ramda';
import AppActions from 'actions/AppActions';
import AppStore from 'stores/AppStore';
import {Row, Col, Panel, ListGroup, ListGroupItem, Media, Button} from 'react-bootstrap';

const getImage = (item, type) => {
    if(type === 'albums' || type === 'artists' || type === 'playlists'){
        if(item.images && item.images.length > 0 ){
            return item.images[0].url;
        }
    }else if(type === 'tracks'){
        if(item.album && item.album.images && item.album.images[0]){
            return item.album.images[0].url;
        }
    }

    return '#';
}

const getDescription = (item, type) => {
    if(type === 'albums' || type === 'tracks'){
        if(!item.artists || item.artists.length === 0){
            return null;
        }
        const artists = [];
        item.artists.forEach((artist) => {
            artists.push(artist.name);
        });
        
        return (
            <p>By: {artists.join(',')}</p>
        );
    }else if(type === 'artists'){
        if(!item.followers){
            return null;
        }

        return (
            <p>Followers: {item.followers.total}</p>
        );
    }else if(type === 'playlists'){
        if(!item.owner){
            return null;
        }

        return (
            <p>Owner: {item.owner.id}</p>
        );
    }
    return null;
}

const capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

const Type = (props) => {
    const results = ramda.clone(props.results);
    let result = <div>No Search Results.</div>;
    if(results.items && results.items.length > 0 ){
        result = results.items.map((item) =>
            <ListGroupItem key={item.id}>
                <Media>
                    <Media.Left>
                        <img width={64} height={64} src={getImage(item, props.name)} alt="Image"/>
                    </Media.Left>
                    <Media.Body>
                        <Media.Heading>
                            {
                                item.external_urls && item.external_urls.spotify ?
                                    <a href={item.external_urls.spotify}>{item.name}</a>
                                : <span>{item.name}</span>
                            }
                        </Media.Heading>
                        {getDescription(item, props.name)}
                    </Media.Body>
                </Media>
            </ListGroupItem>
        );
    }

    return(
        <Row>
            <Col xs={12}>
                <Panel
                    collapsible defaultExpanded 
                    header={`${capitalizeFirstLetter(props.name)} [Results: ${results.total}]`}>
                    <ListGroup fill>
                        {result}
                    </ListGroup>
                {
                    results.previous ? 
                        <Button bsStyle='inverse'
                            onClick={() => {
                                AppActions.getResultsByType(results.previous, props.name,
                                    AppStore.getToken(), AppStore.getTokenType());
                            }}
                        >
                            Previous
                        </Button>
                    : null
                }
                {
                    results.next ? 
                        <Button bsStyle='inverse'
                            onClick={() => {
                                AppActions.getResultsByType(results.next, props.name,
                                    AppStore.getToken(), AppStore.getTokenType());
                            }}
                        >
                            Next
                        </Button>
                    : null
                }
                </Panel>
            </Col>
        </Row>
    );
}

export default Type;