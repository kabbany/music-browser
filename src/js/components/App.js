import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Searchbar from 'components/SearchBar';
import Login from 'components/Login';
import Results from 'components/Results';
import AppStore from 'stores/AppStore';
import AppActions from 'actions/AppActions';
import {Navbar, Grid} from 'react-bootstrap';

class App extends Component{

  constructor(props){
    super(props);
    this.onAppStoreChange = this.onAppStoreChange.bind(this);
  }

  state = {
    gettingToken: true,
  };

  componentDidMount(){
    AppStore.addChangeListener(this.onAppStoreChange);
    const params = this.getHashParams();
    if(params.access_token && params.token_type){
        AppActions.setToken(params.access_token, params.token_type, () => {
          window.location.hash = '';
          this.setState({
            gettingToken: false,
          });
        });
    }else{
      this.setState({
        gettingToken: false,
      });
    }
  }

  onAppStoreChange(){
    this.setState({
      token: AppStore.getToken(),
      results: AppStore.getResults(),
    })
  }

  getHashParams() {
    const hashParams = {};
    let e;
    const r = /([^&;=]+)=?([^&;]*)/g,
        q = window.location.hash.substring(1);
    while ( e = r.exec(q)) {
        hashParams[e[1]] = decodeURIComponent(e[2]);
    }

    return hashParams;
  }

  render(){
    if(this.state.gettingToken){
      return null;
    }
    if(!this.state.token){
      return (
        <Grid>
          <Login/>
        </Grid>
      );
    }
    
    return(
      <div>
          <Navbar inverse collapseOnSelect>
            <Navbar.Header>
              <Navbar.Brand>
                <a href="#">Music Browser</a>
              </Navbar.Brand>
            </Navbar.Header>
          </Navbar>
          <Grid fluid>
            <Searchbar/>
            {
              this.state.results ?
                <Results results={this.state.results}/>
            : null
            }
          </Grid>
      </div>
    );
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('content')
);