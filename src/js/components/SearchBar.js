import React, {Component} from 'react';
import ramda from 'ramda';
import AppStore from 'stores/AppStore';
import AppActions from 'actions/AppActions';
import {Row, Col, Button, FormGroup, FormControl, Checkbox} from 'react-bootstrap';

class SearchBar extends Component{
    constructor(props){
        super(props);
        this.search = this.search.bind(this);
        this.toggleFilter = this.toggleFilter.bind(this);
    }

    state = {
        query : '',
        filters: ['album', 'track', 'artist', 'playlist'],
    };

    toggleFilter(filter){
        const filters = ramda.clone(this.state.filters);
        const indexOfFilter = filters.indexOf(filter);

        if(indexOfFilter > -1){
            filters.splice(indexOfFilter,1);
            this.setState({
                filters,
            });
        }else{
            filters.push(filter);
            this.setState({filters});
        }
    }

    search(){
        if(this.state.query){
            AppActions.getResults(this.state.query, this.state.filters, 
                AppStore.getToken(), AppStore.getTokenType());
        }
    }

    render(){
        return(
            <Row>
                <Col xs={12}>                
                    <Row>
                        <Col xs={8} xsOffset={2} md={6} mdOffset={3}>
                            <Row>
                                <Col xs={8}>
                                    <FormGroup>
                                        <FormControl type="text" 
                                            style={{width:'100%'}}
                                            value={this.state.query} 
                                            type='text' 
                                            onChange={(e) => {
                                                this.setState({
                                                    query: e.target.value,
                                                })
                                            }}
                                            onKeyUp={(e) => {
                                                if(e.key == 'Enter'){
                                                    this.search();
                                                }
                                            }}
                                        />
                                    </FormGroup>
                                </Col>
                                <Col xs={4}>
                                    <Button 
                                        disabled={!this.state.query || this.state.filters.length === 0}
                                        bsStyle='primary' 
                                        onClick={this.search}>
                                        Search
                                    </Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={8} xsOffset={2} md={6} mdOffset={3}>
                            <FormGroup>
                                <Checkbox inline checked={this.state.filters.indexOf('album') > -1}
                                    onChange={() => this.toggleFilter('album')}>
                                    Albums
                                </Checkbox>
                                <Checkbox inline checked={this.state.filters.indexOf('artist') > -1}
                                    onChange={() => this.toggleFilter('artist')}>
                                    Artists
                                </Checkbox>
                                <Checkbox inline checked={this.state.filters.indexOf('track') > -1}
                                    onChange={() => this.toggleFilter('track')}>
                                    Tracks
                                </Checkbox>
                                <Checkbox inline checked={this.state.filters.indexOf('playlist') > -1}
                                    onChange={() => this.toggleFilter('playlist')}>
                                    Playlists
                                </Checkbox>
                            </FormGroup>
                        </Col>
                    </Row>
                </Col>
            </Row>
        );
    }
}

export default SearchBar;