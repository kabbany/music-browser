import React from 'react';
import Type from 'components/Type';
import {Row, Col} from 'react-bootstrap';

const Results = (props) => {
    if(props.results.length === 0 || props.results.error){
        return (<div>No Results found.</div>);
    }
    let count = 0;

    const types = Object.keys(props.results).map((type) => { 
        count += props.results[type].total;   
        return <Type key={type} name={type} results={props.results[type]}/>;
    });

    return(
        <Row>
            <Col xs={8} xsOffset={2} md={6} mdOffset={3}>
                <span>Search results: {count} matches</span>
                {types}
            </Col>
        </Row>
    );
}

export default Results;