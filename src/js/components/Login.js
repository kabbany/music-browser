import React from 'react';
import {Row, Col, Button} from 'react-bootstrap';

const CLIENT_ID = '584c940008d54227aab8910d23551a25';
const REDIRECT_URL = 'http://localhost:5000/';

const Login = () => {
    const login = () => {
        let scope = 'user-read-private user-read-email';
        let url = 'https://accounts.spotify.com/authorize';
        url += '?response_type=token';
        url += '&client_id=' + encodeURIComponent(CLIENT_ID);
        url += '&scope=' + encodeURIComponent(scope);
        url += '&redirect_uri=' + encodeURIComponent(REDIRECT_URL);
        url += '&show_dialog=true';
        window.location = url;
    };

    return(
        <Row>
            <Col xs={4} xsOffset={4} md={2} mdOffset={5}>
                <Button bsStyle='success' onClick={login}>Login with Spotify</Button>
            </Col>
        </Row>
    );
};

export default Login;