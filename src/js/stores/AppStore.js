import BaseStore from './BaseStore';
import AppConstants from 'constants/AppConstants';

class AppStore extends BaseStore{

  constructor(){
    super();
    this.addActionHandler(AppConstants.SET_TOKEN,
      this.setToken.bind(this));
    this.addActionHandler(AppConstants.SET_RESULTS,
      this.setResults.bind(this));
    this.addActionHandler(AppConstants.SET_RESULTS_BY_TYPE,
      this.setResultsByType.bind(this));

    this.registerWithDispatcher();
  }

  getToken(){
    return this.token;
  }
  
  getTokenType(){
    return this.tokenType;
  }

  setToken({token, tokenType, cb}){
    this.token = token;
    this.tokenType = tokenType;
    if(typeof cb === 'function'){
      cb();
    }
    this.emitChange();
  }

  getResults(){
    return this.results;
  }

  setResults({results}){
    this.results = results;
    this.emitChange();    
  }

  setResultsByType({results, type}){
    this.results[type] = results[type];
    this.emitChange();
  }

}

export default new AppStore();
