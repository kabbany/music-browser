import AppDispatcher from 'dispatcher/AppDispatcher';
import AppConstants from 'constants/AppConstants';

const QUERY_LIMIT = 10;

export default {
  setToken(token, tokenType, cb) {
    AppDispatcher.dispatch({
      actionType: AppConstants.SET_TOKEN,
      token,
      tokenType,
      cb,
    });
  },
  getResults(query, filters, token, tokenType){
    fetch(`https://api.spotify.com/v1/search?q=${query}&limit=${QUERY_LIMIT}&type=${filters.join(',')}`,{
        headers:{
            Authorization: `${tokenType} ${token}`,   
        }
    })
    .then((response) => {
        response.json().then((results) => {
            AppDispatcher.dispatch({
              actionType: AppConstants.SET_RESULTS,
              results,
            });
        })
    });
  },
  getResultsByType(url, type, token, tokenType){
    fetch(url,{
        headers:{
            Authorization: `${tokenType} ${token}`,   
        }
    })
    .then((response) => {
        response.json().then((results) => {
            AppDispatcher.dispatch({
              actionType: AppConstants.SET_RESULTS_BY_TYPE,
              results,
              type,
            });
        })
    });
  }
};
