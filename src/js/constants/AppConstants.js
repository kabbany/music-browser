import keyMirror from 'keymirror';

export default keyMirror({
  SET_TOKEN: null,
  SET_RESULTS: null,
  SET_RESULTS_BY_TYPE: null,
});
