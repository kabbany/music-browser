# Music Browser #

This is a simple Spotify search application.

### What are the packages used? ###

* Webpack v2
* React v15.5.4
* Flux v3.1.2
* React Bootstrap v0.31.0

### How do I get set up? ###

#### Using Yarn ####

* yarn
* yarn run build
* yarn run start

#### Using NPM ####

* npm install
* npm run build
* npm run start

#### Launch Application ####
* go to http://localhost:5000 (this is the redirect url stored in the Music Browser Application on Spotify, so the client_id provided in the application will only allow to redirect to this url)

### Comments ###

* The API changed in May 2017 and it does not allow unauthorized search https://developer.spotify.com/migration-guide-for-unauthenticated-web-api-calls/

### To-Do ###

* Store token in localStorage
* Introduce React Router
* Jest
* Redux instead of Flux