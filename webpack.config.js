const path = require('path');
const R = require('ramda');
const resolveAll = paths => R.map(R.partial(path.join)([__dirname]))(paths);

module.exports = {
  entry: './src/js/components/App.js',
  output: {
    filename: 'app.js',
    path: path.resolve(__dirname, 'dist')
  },
  devtool: 'source-map',
  resolve: {
    alias: resolveAll({
      actions: './src/js/actions',
      components: './src/js/components',
      constants: './src/js/constants',
      styles: './src/styles',
      dispatcher: './src/js/dispatcher',
      stores: './src/js/stores',
    }),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env','react','es2015'],
            plugins: [
              require('babel-plugin-transform-class-properties'),
              require('babel-plugin-transform-function-bind')
            ],
          }
        }
      }
    ]
  }
};